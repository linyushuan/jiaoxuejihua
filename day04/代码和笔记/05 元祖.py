# 元组 -- tuple
# 有序,不可变,支持索引
# 元组用于存储一些比较重要的信息
# 元组在配置文件中会使用
# 元组就是不可变的列表

# 定义的方式:

# tu = (1,"alex",True,"大黑哥",[1,2,3])
# lst = [1,"alex",True,"大黑哥",[1,2,3]]
# print(tu)

# tu = (1,"alex",True,"大黑哥",[1,2,3])
# print(tu[0:6])

# for 循环
# for i in tu:
#     print(i)

