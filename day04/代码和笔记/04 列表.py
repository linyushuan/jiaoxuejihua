
# 列表 -- list -- 容器
#     有序,可变,支持索引
# 列表: 存储数据,支持的数据类型很多 字符串,数字,布尔值,列表,集合,元祖,字典,

# 定义一个列表
# lst = ["dsb",123,True,"黑哥"] # 用逗号分隔的是一个元素

# print(lst[0])
# print(id(lst[0]))
# id获取对象的内存地址

# lst[0] = "dsb"
# print(lst)  # ['dsb', 123, True, '黑哥']

# s = "alex" # 字符串是不可变数据
# s = "wusir"
# s[0] = "b"
# print(s)


# 增加:
# lst.append("大煎饼")  #追加(在列表的最后位置添加一个内容)
# print(lst)

# lst.insert(2,"wusir")  #插入  以后尽量不要使用(当数据量比较大的时候会影响效率)
# print(lst)

# lst.extend("可迭代的内容")  # 迭代添加
# print(lst)

# 删除:
# lst.pop()  # 弹 -- 默认删除最后一个
# print(lst)

# lst = ["dsb",123,True,"黑哥"]
# lst.pop(2)   # 通过指定索引进行删除
# print(lst)

# lst = ["dsb",123,"dsb",True,"黑哥"]
# lst.remove("dsb") # 移除 -- 通过元素名字进行删除
# print(lst)

# del lst[0]  # 通过索引删除
# print(lst)

# del lst[0:3] # 通过切片删除
# print(lst)

# del lst[0:3:2] # 通过步长删除
# print(lst)

# lst.clear()  #清空
# print(lst)

# 改:
# lst = ["dsb",123,"dsb",True,"黑哥"]
# lst[1] = "123"
# print(lst)

# lst = ["dsb",123,"dsb",True,"黑哥"]
# lst[1:2] = "12345"
# print(lst)

# lst = ["dsb",123,"dsb",True,"黑哥"]
# lst[1:4] = 12,13,14,15
# print(lst)

# lst = ["dsb",123,"dsb",True,"黑哥"]
# lst[1:4:2] = "12"
# print(lst)  # 步长不为1的必须用一一对应,多一个也不行,少一个也不行

# 查:
# for 循环
# lst = ["dsb",123,"dsb",True,"黑哥"]
# for i in lst:
#     print(i)

# append
# insert
# extend
#
# pop
# remove
# clear
#
# lst[0] = 1
# lst[1:4] = "11111"
# lst[1:4:2] = 1,2
#
# for

# 列表的嵌套:

# lst = [1,"alex","春生","小东北","渣弟",
#        ["大黑哥",["常鑫",["自行车"],"大煎饼","掉井盖","三金"],
#     "冯强","海峰",["太白金星","女神","吴超",["肖锋"]]]]
# print(lst[-1][1][1][0])


# a = lst[-1][-1][-2]
# print(a)
# 不管什么类型进行切片的时候获取都到都是源数据类型
