



# 昨日内容回顾

## 标签操作

### 值操作

```
取值:
	文本输入框:$('#username').val();
	input,type=radio单选框: $('[type="radio"]:checked').val();,首先找到被选中的标签,再进行取值
	input,type=checkbox多选框: 通过val方法不能直接获取多选的值,只能拿到一个,想拿到多个项的值,需要循环取值
		var d = $('[type="checkbox"]:checked');
		for (var i=0;i<d.length;i++){
			console.log(d.eq(i).val());
		}
	单选下拉框select: -- $('#s1').val();
	多选下拉框select: -- $('#s2').val(); -- ['1','2']

设置值
	文本输入框: -- $('#username').val('xxx');
	input,type=radio单选框: -- $(':radio').val(['1']) 找到所有的radio,然后通过val设置值,达到一个选中的效果.
	给单选或者多选框设置值的时候,只要val方法中的值和标签的value属性对应的值相同时,那么这个标签就会被选中.
	此处有坑:$(':radio').val('1');这样设置值,不带中括号的,意思是将所有的input,type=radio的标签的value属性的值设置为1.
	
	input,type=checkbox多选框: -- $(':checkbox').val(['1','2']);
	
	单选下拉框select: -- $('#s1').val(['3']);
	多选下拉框select: -- $('#s2').val(['1','2']);
	
	统一一个方法:
		选择框都用中括号设置值.
```



作业1思路

```
绑定点击事件
1 获取input标签中的值val
2 val().trim().length 
3 =0  
	方式1:提前在input标签后面放一个span标签,加一个类值{color:red},找到span并添加文本内容, .text() .html()
4 如果不等于0,清空span中的内容

```



### 属性操作

```

设置属性: -- $('#d1').attr({'age1':'18','age2':'19'});
		单个设置:$('#d1').attr('age1','18');
查看属性值: -- $('#d1').attr('age1');
删除属性: -- $('#d1').removeAttr('age1'); 括号里面写属性名称
```



```
prop和attr方法的区别:
总结一下：
	1.对于标签上有的能看到的属性和自定义属性都用attr
	2.对于返回布尔值的比如checkbox、radio和option的是否被选中或者设置其被选中与取消选中都用prop。
	具有 true 和 false 两个属性的属性，如 checked, selected 或者 disabled 使用prop()，其他的使用 attr()
	checked示例:
		attr():
            查看值,checked 选中--'checked'  没选中--undefined
                $('#nv').attr({'checked':'checked'}); 
            设置值,attr无法完成取消选中
                $('#nv').attr({'checked':'undefined'});
                $('#nv').attr({'checked':undefined});
                
         prop():
         	查看值,checked 选中--true  没选中--false
         		$(':checkbox').prop('checked');
         	设置值:
         		$(':checkbox').prop('checked',true);
         		$(':checkbox').prop('checked',false);
```



### 文档处理

```

姿势1:添加到指定元素内部的后面
    $(A).append(B)// 把B追加到A
    $(A).appendTo(B)// 把A追加到B
    
	append示例:
        方式1: 
            创建标签
                var a = document.createElement('a');
                $(a).text('百度');
                $(a).attr('href','http://www.baidu.com');
                $('#d1').append(a);
        方式2:(重点)
            $('#d1').append('<a href="xx">京东</a>');
	appendto示例
		$(a).appendTo('#d1');
		
姿势2:添加到指定元素内部的前面
	$(A).prepend(B)// 把B前置到A
	$(A).prependTo(B)// 把A前置到B
姿势3:添加到指定元素外部的后面
	$(A).after(B)// 把B放到A的后面
	$(A).insertAfter(B)// 把A放到B的后面
姿势4:添加到指定元素外部的前面
	$(A).before(B)// 把B放到A的前面
	$(A).insertBefore(B)// 把A放到B的前面

移除和清空元素
	remove()// 从DOM中删除所有匹配的元素。
	empty()// 删除匹配的元素集合中所有的子节点，包括文本被全部删除，但是匹配的元素还在
	示例:
		$('#d1').remove();
		$('#d1').empty();
		
替换:
	replaceWith()
	replaceAll()
     示例:
     	$('#d1').replaceWith(a);  用a替换前面的标签
     	$(a).replaceAll('#d1');   
     
```



克隆(复制标签)

```
示例:
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>

	<button class="btn">屠龙宝刀,点击就送!</button>

</body>
<script src="jquery.js"></script>
<script>
    $('.btn').click(function () {
        // var btnEle = $(this).clone(); // 不带参数的克隆不能克隆绑定的事件
        var btnEle = $(this).clone(true); // 参数写个true,就能复制事件
        $(this).after(btnEle);

    })

</script>


</html>
```



作业3

```
1 模态对话框结合点击事件完成弹出和隐藏
2 点击取消,关闭对话框
3 点击确认,关闭对话框,获取用户输入的内容
4 拼接一个tr标签,将数据放到里面的td标签里面,然后将tr标签放到tbody标签内部的后面

5 开除,点击这一行的那个开除按钮,就将本行tr标签删除 remove方法
```



### 事件

```
绑定事件的两种方式:
	// 绑定事件的方式1
    // $("#d1").click(function () {
    //     $(this).css('background-color','green');
    // })

    // 方式2
    $('#d1').on('click',function () {
        $(this).css('background-color','green');
    })
```



常用事件



```

click(function(){...})
hover(function(){...})
blur(function(){...})
focus(function(){...})
change(function(){...}) //内容发生变化，input，select等
keyup(function(){...})  
mouseover 和 mouseenter的区别是：mouseover事件是如果该标签有子标签，那么移动到该标签或者移动到子标签时会连续触发，mmouseenter事件不管有没有子标签都只触发一次，表示鼠标进入这个对象

```

```
示例:
	<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Title</title>
        <style>
            #d1{
                background-color: red;
                height: 200px;
                width: 200px;
            }
            #d2{
                background-color: green;
                height: 200px;
                width: 200px;
            }

            .dd1{
                background-color: yellow;
                height: 40px;
                width: 40px;
            }
            .dd2{
                background-color: pink;
                height: 40px;
                width: 40px;
            }

        </style>
    </head>
    <body>



    <div id="d1">
        <div class="dd1"></div>
    </div>
    <div id="d2">
        <div class="dd2"></div>
    </div>

    用户名:<input type="text" id="username">


    <br>

    <!--<select name="" id="s1">-->
    <!--    <option value="1">上海</option>-->
    <!--    <option value="2">深圳</option>-->
    <!--    <option value="3">贵州</option>-->
    <!--</select>-->

    <input type="text" id="xxx">

    </body>
    <script src="jquery.js"></script>
    <script>
        // 绑定事件的方式1
        // $("#d1").click(function () {
        //     $(this).css('background-color','green');
        // })

        // 方式2
        // $('#d1').on('click',function () {
        //     $(this).css('background-color','green');
        // });
        //
        // // 获取光标触发的事件
        // $('#username').focus(function () {
        //     $(this).css('background-color','green');
        // });
        // // 失去光标触发的事件
        // $('#username').blur(function () {
        //     $(this).css('background-color','white');
        // });
        // // 域内容发生变化触发的事件,一般用于select标签
        // $('#s1').change(function () {
        //      // $('#d1').css('background-color','black');
        //     console.log('xxxxx')
        //
        // });

        // $('#xxx').change(function () {
        //     console.log($(this).val());
        // })

        // 输入内容实时触发的事件,input事件只能on绑定
        // $('#xxx').on('input',function () {
        //     console.log($(this).val());
        // });
        //
        // //绑定多个事件 事件名称空格间隔
        // $('#xxx').on('input blur',function () {
        //     console.log($(this).val());
        // })


        // 鼠标进入触发的事件
        // $('#d1').mouseenter(function () {
        //     $(this).css('background-color','green');
        // });
        // // 鼠标离开触发的事件
        // $('#d1').mouseout(function () {
        //     $(this).css('background-color','red');
        // });

        // hover事件 鼠标进进出出的事件
        // $('#d1').hover(
        //     // 鼠标进入
        //     function () {
        //         $(this).css('background-color','green');
        //     },
        //     // 鼠标离开
        //     function () {
        //         $(this).css('background-color','red');
        //     }
        // );


        $('#d1').mouseenter(function () {
            console.log('xxx');
        });
        $('#d2').mouseover(function () {
            console.log('ooo');
        });
		// 键盘按下
		   // $(window).keydown(function (e) {
            //     console.log(e.keyCode);
            //
            // });
         // 键盘抬起
            $(window).keyup(function (e) {
                console.log(e.keyCode);
            });

    </script>
    </html>
```



### 移除事件（不常用）

```
.off( events [, selector ][,function(){}])
off() 方法移除用 .on()绑定的事件处理程序。

$("li").off("click")；就可以了
```



### 事件冒泡

```
    // 事件冒泡,子标签和父标签(祖先标签)绑定了相同的事件,比如点击事件,那么当你点击子标签时,会一层一层的往上触发父级或者祖父级等等的事件
    $('.c1').click(function () {
        alert('父级标签!!!');

    });
    $('.c2').click(function (e) {
        alert('子标签~~~');
        // 阻止事件冒泡(阻止事件发生)
        return false; //方式1
        // e.stopPropagation() // 方式2
    })
```

### 事件委托

```

	<div id="d1">
    	<button class="btn">屠龙宝刀,点击就送!</button>.
    	
	</div>

    // 事件委托
    $('#d1').on('click','.btn',function () {
          // $(this)是你点击的儿子标签
        var a= $(this).clone();
        $('#d1').append(a);
    });
//中间的参数是个选择器，前面这个$('table')是父级标签选择器，选择的是父级标签，意思就是将子标签（子子孙孙）的点击事件委托给了父级标签
//但是这里注意一点，你console.log(this)；你会发现this还是触发事件的那个子标签，这个记住昂
```



# 今日内容



## jquery



### 页面载入

```
window.onload:
	原生js的window.onload事件:// onload 等待页面所有内容加载完成之后自动触发的事件
	window.onload = function(){
            $('.c1').click(function () {
                $(this).addClass('c2');
            });

        };


jquery页面载入:
	$(function () {
            $('.c1').click(function () {
                $(this).addClass('c2');
            });
        })

非简写方式
	$(document).ready(function(){
        // 在这里写你的JS代码...
     })

与window.onload的区别
	1.window.onload()函数有覆盖现象，必须等待着图片资源加载完成之后才能调用
	2.jQuery的这个入口函数没有函数覆盖现象，文档加载完成之后就可以调用（建议使用此函数）


```



### jquery的each

```
循环数组:
	var a = [11,22,33];
    $.each(a,function(k,v){
       console.log(k,v);
    })

循环标签对象:
	$('li').each(function(k,v){
        console.log(k,$(v).text());
    })

return false；终止循环

在遍历过程中可以使用 return false提前结束each循环。 类似于break
而直接使用return;后面什么都不加，不写false，就是跳过本次循环的意思 类似与continue

```



### bootstrap

#### 栅格系统

#### 列偏移col-md-offset-x

```
col-md-offset-x
    <div class="container-fluid">
<!--            <h1>xxxx</h1>-->
        <div class="row">
            <div class="col-md-6 col-md-offset-3 c1">


        </div>


    </div>
```

### 列嵌套

```
    <div class="container-fluid">
<!--            <h1>xxxx</h1>-->
        <div class="row">
            <div class="col-md-6 col-md-offset-3 c1">
            // 列中又分栅格
                <div class="row">
                    <div class="col-sm-6 c1"></div>
                    <div class="col-sm-6 c2"></div>
                </div>


            </div>

        </div>


    </div>
```





















































































































