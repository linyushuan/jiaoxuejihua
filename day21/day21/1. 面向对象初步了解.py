# 面向过程编程vs函数
# s1 = 'fdskajfdfsdfs'
# count = 0
# for i in s1:
#     count += 1
# print(count)
#
#
# l1 = [i for i in range(10)]
#
# count = 0
# for i in l1:
#     count += 1
# print(count)
#
#
# l1 = [i for i in range(10)]
# def my_len(argv):
#     count = 0
#     for i in argv:
#         count += 1
#     print(count)
#
# my_len(l1)

# 减少了重复代码,增强了可读性


# 函数式编程vs面向对象编程

def login():
    pass


def register():
    pass



def shopping_car():
    pass



def change_pwd():
    pass




def check_paid_goods():
    pass


def check_unpaid_goods():
    pass





class Auth:

    def login(self):
        pass

    def register(self):
        pass

    def change_pwd(self):
        pass


class Shopping:

    def shopping_car(self):
        pass

    def check_paid_goods(self):
        pass

    def check_unpaid_goods(self):
        pass


# 面向对象第一个优点: 对相似功能的函数,同一个业务下的函数进行归类,分类.


'''
想要学习面向对象必须站在一个上帝的角度去分析考虑问题.
    类: 具有相同属性和功能的一类事物.
    对象:某个类的具体体现.

汽车: 汽车类, 楼下停着一个车牌号为9nb11的奥迪对象.
猫科类: 类. 陈硕家的养的那个大橘.对象.
鸡类: 一个类. 家里养的一只鸡.
男神:是一类. 太白对象.

面向对象的第二优点: 你要站在上帝的角度构建代码,类就是一个公共的模板,对象就是从模板实例化出来的.
得到对象就得到了一切.
'''
