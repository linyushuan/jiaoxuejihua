# len()
# s = "alexdab"
# count = 0
# for i in s:
#     count += 1
# print(count)
#
# s = [1,2,23,3,4,5,6]
# count = 0
# for i in s:
#     count += 1
# print(count)
#
#
# s = (1,2,23,3,4,5,6)
# count = 0
# for i in s:
#     count += 1
# print(count)

# def func():
#
#     s = (1,2,23,3,4,5,6)
#     count = 0
#     for i in s:
#         count += 1
#     print(count)

# def 关键字 -- 定义
# func 函数名 -- 和变量定义规则一样
# ()   必须要写格式 ,一会在盘他
# : 声明语句结束

# 定义一个函数:

# def my_len():
#     函数体

# def my_len():
#     s = (1,2,23,3,4,5,6)
#     count = 0
#     for i in s:
#         count += 1
#     print(count)

# 调用函数
# my_len() # 函数名+() print() input()


# def yue():
#     print("打开手机")
#     print("打开微信")
#     print("找到女友")
#     print("晚上约吗")

# def yue():
#     print("打开手机")
#     print("打开微信")
#     print("找到女友")
#     print("晚上约吗")


# print("起床")
# print("上课")
# yue()
# print("吃饭")
# print("检查一下")
# yue()

# 函数的返回值



# print(12)
# def yue():
#     print("打开手机")
#     print("打开微信")
#     return 111
#     print("找到女友")
#     print("晚上约吗")
#     return "萝莉小姐姐","大妈","人妖"  # return 返回
#     print("确实有点累")
#
# print(yue())  # 111
# print(1234)

# lst = [1,2,3,4,5,6]
# def func():
#     for i in lst:
#         print(i)
# print(func())

# lst = [1,2,3,4,5]
# def func():
#     for i in lst:
#         print(i)
#     lst.append(input(">>>"))
#     return lst
#
# func() # [1,2,3,4,5,"9"]


# return的内容返回给了函数的调用
# return下方的代码不执行,终止这个函数
# return 返回多个内容的时候还是元组形式
# return 没有写返回值的时候返回的是None,不写return返回的也是None


# 位置传参:
# def yue(a,b,c):  # 形参
#     print("打开手机")
#     print(f"打开{a},{c},{b}")
#     print("附近的人")
#     print("找个妹子")
#     print("聊一聊")
#
# yue(True,(12,4,3),[1,2,3,4]) # 实参

# def func(a,b=1): #b=1 默认参数
#     print(a,b)
# func(3,8)

# 姓名,性别,年龄
# def userinfo(name,age,sex="男"):  # 位置参数 > 默认参数
#     print(name,sex,age)
#
# count = 5
# while count:
#     name = input("name:")
#     sex = input("sex(男性直接回车!):")
#     age = input("age:")
#     if sex == "女":
#         userinfo(name,age,sex)
#     else:
#         userinfo(name, age)
#     count -= 1

# def func(a,c,b=1): # a,c位置参数 ,b=1 是默认参数
#     print(a,b,c)
#
# func(a=1,b=5,c=2) # a=1,b=5,c=2 是关键字参数

# def func(a,b,c):
#     print(a,b,c)
# func(1,2,c=5) # 混合参数

# 形参: 函数的定义中括号里是形参
# 实参: 函数的调用括号里是实参
# 位置传参时 形参和实参必须一一对应
# 传参: 将实参传递给形参的过程就是传参

# 函数的参数:
    # 形参: 函数定义的时候叫做形参
        # 位置参数
        # 默认参数
        # 混合参数

    # 实参: 函数调用的时候叫做实参
        # 位置参数
        # 关键字参数
        # 混合参数

    # 传参: 将实参传递给形参的过程叫做传参