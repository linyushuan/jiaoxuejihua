
import pymysql

conn = pymysql.connect(
    host='127.0.0.1',
    port=3306,
    user='root',
    password='666',
    database='day43',
    charset='utf8',

)

while 1:
    username = input('请输入用户名:')
    password = input('请输入密码:')

    cursor = conn.cursor(pymysql.cursors.DictCursor)
    # sql注入
    # sql = "select * from userinfo where username='%s' and password='%s';" \
    #       % (username, password)
    # # sql = "select * from userinfo where username='duijie '-- ' and password='%s';" \
    # #       % (username, password) #知道用户名不知道密码,登录网站
    # sql = "select * from userinfo where username='asdfasdf' or 1=1 -- ' and password='%s';" \
    #       % (username, password) #不知道用户名也不知道密码,登录网站
    # pymysql解决sql注入问题
    sql = "select * from userinfo where username=%s and password=%s;"
    ret = cursor.execute(sql,[username,password])
    if ret:
        print('登录成功')
    else:
        print('账号或者密码错误,请重新输入!!!')

# 增删改都必须进行提交操作(commit)
conn.commit()








