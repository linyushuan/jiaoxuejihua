# 运用类完成一个扑克牌类(无大小王)的小游戏：
# 用户需要输入用户名，以下为用户可选选项:
#     1. 洗牌
#     2. 随机抽取一张
#     3. 指定抽取一张
#     4. 从小到大排序
#     5. 退出
#
# 1. 洗牌：每次执行的结果顺序随机。
# 2. 随机抽取一张：显示结果为：太白金星您随机抽取的牌为：黑桃K
# 3. 指定抽取一张：
#     用户输入序号（1~52）
#     比如输入5，显示结果为：太白金星您抽取的第5张牌为：黑桃A
# 4. 将此牌从小到大显示出来。A -> 2 -> 3 .......-> K
#
# 提供思路：
#     52张牌可以放置一个容器中。
#     用户名，以及盛放牌的容器可以封装到对象属性中。
import random

class Poker:

    num_list = ['A'] +[i for i in range(2,11)] + list('JQK')
    color_list = '黑桃 红桃 方片 梅花'.split()

    def __init__(self,name):
        self.name = name
        self.card = [(color, num) for color in self.color_list for num in self.num_list]


    def shuffle(self):
        random.shuffle(self.card)
        return self.card

    def _random(self):
        random_card = random.choice(self.card)
        return  f'{self.name} 您随机抽取的牌为：{random_card[0]}{random_card[1]}'

    def appoint(self):
        appoint_num = int(input('抽取第几张?(1~52)'))
        return f'{self.name} 您抽取的第{appoint_num}张牌为：{self.card[appoint_num-1][0]}{self.card[appoint_num-1][1]}'

    def sort(self):
        self.card = [(color, num) for num in self.num_list for color in self.color_list]
        return [(color, num) for num in self.num_list for color in self. color_list]

    def exit(self):
        exit()


choice_dict = {
    1: Poker.shuffle,
    2: Poker._random,
    3: Poker.appoint,
    4: Poker.sort,
    5: Poker.exit,

}

while 1:
    name = input('请输入用户名').strip()
    obj = Poker(name)
    while 1:
        choice_num = input('''
        1. 洗牌
        2. 随机抽取一张
        3. 指定抽取一张
        4. 从小到大排序
        5. 退出
        ''').strip()
        try:
            print(choice_dict[int(choice_num)](obj))
        except Exception :
            print('输入有误')


    # class A:
    #
    #     def func(self):
    #         print(self)
    #
    # # A.func(11)
    # obj = A()
    # obj.func()
    # A.func(obj)
