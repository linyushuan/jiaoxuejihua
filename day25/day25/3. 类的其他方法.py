# 类方法

class A:

    def func(self):
        print('实例方法')


    @classmethod
    def cls_func(cls):
        # print(f'cls---->{cls}')
        obj = cls()
        print(obj)
        print('类方法')

# print(A)
# A.cls_func()
# obj = A()
# obj.cls_func()

# 类方法: 一般就是通过类名去调用的方法,并且自动将类名地址传给cls,
# 但是如果通过对象调用也可以,但是传的地址还是类名地址.
A.cls_func()

# 类方法有什么用???
#     1. 得到类名可以实例化对象.
#     2. 可以操作类的属性.

# 简单引用
# 创建学生类,只要实例化一个对象,写一个类方法,统计一下具体实例化多少个学生?


# class Student:
#
#     count = 0
#     def __init__(self,name,id):
#
#         self.name = name
#         self.id = id
#         Student.addnum()
#
#     @classmethod
#     def addnum(cls):
#         cls.count = cls.count + 1
#
#     @classmethod
#     def getnum(cls):
#         return cls.count
#

# obj1 = Student('liye', 12343243243)
# obj1 = Student('liye', 12343243243)
# obj1 = Student('liye', 12343243243)
# obj1 = Student('liye', 12343243243)
# obj1 = Student('liye', 12343243243)
# obj1 = Student('liye', 12343243243)
# obj1 = Student('liye', 12343243243)
# obj1 = Student('liye', 12343243243)
# obj1 = Student('liye', 12343243243)
# obj1 = Student('liye', 12343243243)
# obj1 = Student('liye', 12343243243)
# obj1 = Student('liye', 12343243243)
# obj1 = Student('liye', 12343243243)
#
# print(Student.getnum())


# 静态方法



# class A:
#
#     def func(self):
#         print('实例方法')
#
#
#     @classmethod
#     def cls_func(cls):
#         pass
#
#     @staticmethod
#     def static_func():
#         print('静态方法')
#
# # 静态方法是不依赖于对象与类的,其实静态方法就是函数.
# 保证代码的规范性,合理的划分.后续维护性高.
# def func():
#     pass


import time

class TimeTest(object):

    area = '中国'
    def __init__(self, hour, minute, second):
        self.hour = hour
        self.minute = minute
        self.second = second

    def change_time(self):
        print(f'你想调整的时间: {self.hour}时{self.minute}分{self.second}秒')

    @staticmethod
    def showTime():
        return time.strftime("%H:%M:%S", time.localtime())


def showTime():
    return time.strftime("%H:%M:%S", time.localtime())

def time1():
    pass

def time2():
    pass
# t = TimeTest(2, 10, 10)
# # t.change_time()
# print(TimeTest.showTime())