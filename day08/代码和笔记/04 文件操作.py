# "萝莉小姐姐电话号.txt"
# open()  # 打开
# file  # 文件的位置(路径)
# mode  # 操作文件的模式
# encoding # 文件编码方式
# f  # 文件句柄

# f = open("萝莉小姐姐电话号",mode="r",encoding="utf-8")
# print(f.read())
# f.close()

# 操作文件:
    # 1. 打开文件
    # 2. 操作文件
    # 3. 关闭文件

# 文件操作的模式:
# r,w,a (重要)
# rb,wb,ab (次要)
# r+,w+,a+ (没啥用)

# f = open("文件的路径(文件放的位置)",mode="操作文件的模式",encoding="文件的编码") # 内置函数
# f(文件句柄)

