1. 今日内容大纲

   1. 回顾知识点

   2. 为什么要有规范化目录

   3. 分析规范化目录.

   4. 实战演练.

   5. logging日志的进阶版.

      

2. 函数以及模块的回顾

3. 今日内容

   1. 回顾知识点

   2. 为什么要有规范化目录

      py 几百行,以后的项目,

      代码分类:

      ​	加载快.

      ​	可读性高.

      ​	查询修改都简单.

      

   3. 分析规范化目录.

      1. 划归固定的路径:

         ![1564629036767](C:\Users\Administrator\Documents\assets\1564629036767.png)

      2. settings.py文件.

         ![1564629460486](C:\Users\Administrator\Documents\assets\1564629460486.png)

      3. src.py 主逻辑核心逻辑文件,common.py 公共组件部分

         ![1564629992366](C:\Users\Administrator\Documents\assets\1564629992366.png)

      4. start.py文件

         ![1564630487254](C:\Users\Administrator\Documents\assets\1564630487254.png)

      5. 类似于register文件: 用户信息,数据相关,多个文件.

      6. logging日志文件: 记录用户的访问次数,转账,取钱,充钱等等.极了用户干了什么.

         ![1564631262778](C:\Users\Administrator\Documents\assets\1564631262778.png)

      

   4. 实战演练.

      将我们的单个文件博客园作业,按照规范化目录重新构建.

      ![1564636024049](C:\Users\Administrator\Documents\assets\1564636024049.png)

      ![1564636060653](C:\Users\Administrator\Documents\assets\1564636060653.png)

      ![1564636437194](C:\Users\Administrator\Documents\assets\1564636437194.png)

   5. logging日志的进阶版.

      

4. 今日总结

   分目录:将一个py文件,合理的分成6个文件.以及可以成功运行起来.

   模块如何使用,. sys.path作用, 需要哪个文件的什么功能,就去那个文件引用.

   

5. 明日预习