"""
1.请实现一个装饰器，限制该函数被调用的频率，如10秒一次（面试题）
答
"""
# import time
# def wrapper(func):
#     t = 0
#     def inner(*args,**kwargs):
#         nonlocal t
#         if time.time() - t >= 2:
#             t = time.time()  # 记录第一次执行的时间
#             func(*args,**kwargs)
#         else:
#             print("被限制啦")
#     return inner
#
# @wrapper
# def foo():
#     print("这是一个被装饰的函数")
#
# while True:
#     foo()
#     time.sleep(1)
#     foo()

"""
用户有两套账号密码,一套为京东账号密码，一套为淘宝账号密码分别保存在两个文件中。
设置四个函数，分别代表 京东首页，京东超市，淘宝首页，淘宝超市。
启动程序后,呈现用户的选项为:
1,京东首页
2,京东超市
3,淘宝首页
4,淘宝超市
5,退出程序
四个函数都加上认证功能，用户可任意选择,用户选择京东超市或者京东首页,只要输入一次京东账号和密码并成功,
则这两个函数都可以任意访问;用户选择淘宝超市或者淘宝首页,只要输入一次淘宝账号和密码并成功,
则这两个函数都可以任意访问.
相关提示：用带参数的装饰器。装饰器内部加入判断，验证不同的账户密码。
"""

msg = """
1,京东首页
2,京东超市
3,淘宝首页
4,淘宝超市
5,退出程序
>>>
"""


# login_dic = {
#     "jd":False,
#     "tb":False
# }
#
# def auth(argv):
#     def wrapper(func):
#         def inner(*args,**kwargs):
#             if login_dic[argv]:
#                 func(*args,**kwargs)
#             else:
#                 user = input('username:')
#                 pwd = input('password:')
#                 with open(argv,"r+",encoding="utf-8")as f:
#                     for i in f:
#                         username,password = i.strip().split(":")
#                         if user == username and pwd == password:
#                             login_dic[argv] = True
#                             print("登录成功!")
#                             break
#                     else:
#                         print("用户名或密码错误!")
#         return inner
#     return wrapper
#
#
# @auth("jd")
# def jd_index():
#     pass
#
#
# @auth("jd")
# def jd_shopping():
#     pass
#
#
# @auth("tb")
# def tb_index():
#     pass
#
#
# @auth("tb")
# def tb_shopping():
#     pass
#
#
# func_dic = {"1":jd_index,"2":jd_shopping,"3":tb_index,"4":tb_shopping,"5":exit}
# choose = input(msg)
# if choose in func_dic:
#     func_dic[choose]()
# else:
#     print("请正确输入!")

# 4.给l1 = [1,1,2,2,3,3,6,6,5,5,2,2]去重，不能使用set集合（面试题）。

"""
5.用递归函数完成斐波那契数列（面试题）：
斐波那契数列：1，1，2，3，5，8，13，21..........(第三个数为前两个数的和，
但是最开始的1，1是特殊情况，可以单独讨论)
用户输入序号获取对应的斐波那契数字：比如输入6，返回的结果为8.

"""
# [1,1,2,3,5,8]

# def func(num):  # 类似于索引,从一开始数
#     if num == 0:
#         ret = 0
#     elif num == 1:
#         ret = 1
#     else:
#         ret = func(num - 2) + func(num - 1)
#     return ret
# print(func(20))


