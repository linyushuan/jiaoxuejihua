# 1.有参装饰器

# login_dic = {
#     "username": None,
#     "flag": False
# }
# def auth(argv):
#     def wrapper(func):
#         def inner(*args,**kwargs):
#             if login_dic["flag"]:
#                 func(*args,**kwargs)
#             else:
#                 if argv == "QQ":
#                     print("欢迎登陆QQ")
#                     user = input("username:")
#                     pwd = input("password:")
#                     if user == "alex" and pwd == "alex123":  # qq
#                         login_dic["flag"] = True
#                         login_dic["username"] = user
#                         func(*args,**kwargs)
#                     else:
#                         print("用户名或密码错误!")
#         return inner
#     return wrapper
# @auth("QQ")
# def foo():
#     print("这是一个被装饰的函数")

# 2.多个装饰器装饰一个函数
def wrapper1(func):
    def inner1(*args,**kwargs):
        print(1)
        func(*args,**kwargs)
        print(11)
    return inner1

def wrapper2(func):  # func == foo
    def inner2(*args,**kwargs):
        func(*args, **kwargs)
        print(22)
    return inner2

def wrapper3(func):
    def inner3(*args,**kwargs):
        print(3)
        func(*args, **kwargs)
        print(33)
    return inner3

# @wrapper1
# @wrapper3
# @wrapper2
# def foo():
#     print(8)

# 先执行离被装饰函数最近的语法糖

# inner2 = wrapper2(foo)
# inner3 = wrapper3(inner2)
# foo = wrapper1(inner3)