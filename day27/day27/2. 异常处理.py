# 1. 错误分类
#   语法错误.
# if 2 > 1
#     print(222)
#
# dic = {'name'; 'alex'}
# tu = (2, 3, 4'' 6 )

# 逻辑错误
# num = int(input('请输入数字'))
# dic = {'name': '海狗', 'age': 18}
# dic['hobby']

# 进行一个try举例;

# try:
#     num = int(input('>>>'))  # 出现ValueError错误之后,直接跳转到except语句.
#     print(111)
# except ValueError:
#     print(666)


# try:
#     dic = {'name': '嘉欣'}
#     print(dic['age'])
#     num = int(input('>>>'))  # 出现ValueError错误之后,直接跳转到except语句.
#     print(111)
# except ValueError:
#     print(666)

# 结构1:单分支

# try:
#     num = int(input('>>>'))  # 出现ValueError错误之后,直接跳转到except语句.
#     dic = {'name': '嘉欣'}
#     print(dic['age'])
#
#     print(111)
# except ValueError:
#     print(666)


# 结构2: 多分支

# try:
#     num = int(input('>>>'))  # 出现ValueError错误之后,直接跳转到except语句.
#     dic = {'name': '嘉欣'}
#     print(dic['age'])
#     l1 = [1, 2]
#     print(l1[100])
#     print(111)
# except ValueError:
#     print('输入的有非数字元素')
# except KeyError:
#     print('没有此键')
# except IndexError:
#     print('没有此下标')
# print(666)


# 结构3: 万能异常: 处理所有pyhton识别的异常.

# try:
#
#     dic = {'name': '嘉欣'}
#     # print(dic['age'])
#     l1 = [1, 2]
#     print(l1[100])
#     print(111)
#     for i in 123:
#         pass
#
# except Exception as e:
#     print(e)
# print(666)

# 什么时候用万能? 什么时候用多分支?
# 如果你对错误信息不关心,只是想要排除错误让程序继续运行. 用万能异常.
# 你对错误信息要进行明确的分流,让你的程序多元化开发.

# 之前的写法:
# num = input('输入序号')
# if num.isdecimal():
#     num = int(num)
#     if 0 < num < 5:
#         if num == 1:
#             pass # func()
# else:
#     print('请输入数字')

# def func():
#     pass
#
# def func1():
#     pass
#
# dic = {
#     1: func,
#     2: func1,
# }
#
# try:
#     num = int(input('请输入序号'))
#     dic[num]()
# except ValueError:
#     print('请输入数字')
# except KeyError:
#     print('请输入范围内的序号')
#
# 结构4: 多分支+万能异常

# def func():
#     pass
#
#
# def func1():
#     pass
#
#
# dic = {
#     1: func,
#     2: func1,
# }
#
# try:
#     num = int(input('请输入序号'))
#     dic[num]()
# except ValueError:
#     print('请输入数字')
# except KeyError:
#     print('请输入范围内的序号')
# except Exception:
#     print('程序出现意料之外的错误....')

# 结构5: try else finally
#
# try:
#     dic = {'name': '嘉欣'}
#     # print(dic['age'])
#     l1 = [1, 2]
#     # print(l1[100])
#     print(111)
# except KeyError:
#     print('没有此键')
# except IndexError:
#     print('没有此下标')
# else:
#     print('如果没有出现异常则执行这里')
# finally:
#     print('finally 666')

# except 必须依赖于try, else必须依赖于except和try
# finally只是依赖于try.

# finally : 在异常出现之前,执行finally语句.
# try:
#     dic = {'name': '嘉欣'}
#     print(dic['age'])
#     l1 = [1, 2]
#     print(l1[100])
# # except KeyError:
# #     print('没有此键')
# # except IndexError:
# #     print('没有此下标')
# # except IndexError:
# #     pass
# # print(111)
# finally:
#     print('finally 666')

# finally 用在 关闭数据库连接,文件句柄关闭,数据保存等,用到finally.

# with open('test1',encoding='utf-8',mode='r+') as f1:
#     try:
#         for i in f1:
#             print(i)
#         if ....:
#     finally:
#
#         f1.close()

# 在return结束函数之前,执行finally代码.
# def func():
#     try:
#         print(111)
#         return 666
#     finally:
#         print(222)
# print(func())

# 结构6: 主动触发异常.
# raise ValueError('出现了value错误')

# 结构7: 断言: 展现出一种强硬的态度.

# assert 条件
# name = 'alex'
# n1 = input('请输入:')
# assert name == n1
# print(111)
# print(222)

# 自定义异常
# python中给你提供的错误类型很多,但是不是全部的错误.

# class LiYeError(BaseException):
#
#     def __init__(self,msg):
#         self.msg=msg
#     def __str__(self):
#         return self.msg
#
# try:
#     # 三行
#     raise LiYeError('socket.connent.....')
# except LiYeError as e:  # e = LiYeError('类型错误')
#     print(e)

#   socket.connent .....

# EOFError
# ValueError