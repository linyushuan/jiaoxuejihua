# 昨日内容回顾

HTML文档结构

```
标签要封闭,全封闭,自封闭

html文件不识别多个空格或者换行,都识别成一个空格

注释:
	<!-- 注释 -->

```

head标签

```

网页源信息,配置信息
title标签,标签页显示内容

```

body标签

```

h1 - h6
b,i,u,s,p,br,hr
```

# 今日内容

特殊符号 

```
&nbsp;--空格
&gt;大于号
&lt;小于号
... 
找HTML特殊符号
```

标签分类

```

块级标签(行外标签):独占一行,可以包含内敛标签和某些块级标签,div,p,h1-h6,hr,form
内敛标签(行内标签):不独占一行,不能包含块级标签,只能包含内敛标签 b,i,u,s,a,img,select,input,span,textarea
p标签,p标签内部不能包含块级标签和p标签

```

img标签和a标签

```
img标签  内敛标签
	<img src="1.png" alt="这是个美女..." width="200" height="200" title="美女">
	src属性='图片路径' alt属性='图片加载失败的提示信息'  title='鼠标悬浮提示信息'(title属性不单单是img标签的)
```

a标签(超链接标签)

```
内联标签
a标签 
	没有写href属性,<a>百度</a>  显示普通文本
	有属性,但是没有值:<a href="">百度</a>  #href="" ,写了href属性,但是里面没有值,效果:有特殊效果,文字有颜色,有下划线,点击会刷新当前页面
	有属性有值的:<a href="http://www.baidu.com">百度</a>,跳转到href属性指定的网址

<a href="http://www.jd.com" target="_blank">京东</a>
target="_blank"  新的标签页打开,默认是在当前标签页打开
target="_self"   默认的,在当前标签页打开

```

​	锚点示例:

```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        .c1{
            height: 1000px;
            width: 200px;
            background-color: red;
        }
    </style>
</head>
<body>
<!-- 设置锚点 方式1 -->
<a name="top">这是顶部</a>
<!-- 方式2 <div id="top">div顶部</div> -->
<h1>24期皇家赌场</h1>

<!--<div id="top">这是顶部</div>-->
<img src="1.png" alt="这是个美女..." width="200" height="200" title="美女">

<a href="">百度</a>
http://www.baidu.com
<a href="http://www.jd.com" target="_blank">京东</a>

<div class="c1"></div>

#跳转锚点,写的是a标签name属性对应的值,写法 href='#值'
<a href="#top">回到顶部</a>


</body>
</html>
```

列表标签

type对应的值:

​	

```
无序列表type属性对应的值：

disc（实心圆点，默认值）
circle（空心圆圈）
square（实心方块）
none（无样式）
```

```
有序列表type属性对应的值： start属性是从数字几开始

1 数字列表，默认值
A 大写字母
a 小写字母
Ⅰ大写罗马
ⅰ小写罗马
```

```
有序和无序列表:type属性控制显示样式,start控制起始值
无序列表:
<ul type="none">
    <li>柴嘉欣</li>
    <li>周道镕</li>
    <li>朱凡宇</li>

</ul>

有序列表
<ol type="I" start="2">
    <li>柴嘉欣</li>
    <li>周道镕</li>
    <li>朱凡宇</li>

</ol>

标题列表
<dl>
  <dt>标题1</dt>
  <dd>内容1</dd>
  <dt>标题2</dt>
  <dd>内容1</dd>
  <dd>内容2</dd>
</dl>
```

table标签(重点)

```
<table border="1" cellspacing="10" cellpadding="20">
    <thead>
        <tr>
            <th>姓名</th>
            <th>年龄</th>
            <th>爱好</th>
        </tr>
    </thead>
    <tbody>
    <tr>
        <td colspan="2">柴嘉欣</td>
<!--        <td>18</td>-->
        <td rowspan="3">篮球</td>

    </tr>
    <tr>
        <td>周道镕</td>
        <td>18</td>
<!--        <td>篮球</td>-->

    </tr>
    <tr>
        <td>朱凡宇</td>
        <td>18</td>
<!--        <td>篮球</td>-->
    </tr>
    </tbody>

</table>

```

```
常用属性:	
border: 表格边框.
cellpadding: 内边距 （内边框和内容的距离）
cellspacing: 外边距.（内外边框的距离）
width: 像素 百分比.（最好通过css来设置长宽）
rowspan: 单元格竖跨多少行
colspan: 单元格横跨多少列（即合并单元格）
```

input标签

| type属性值 | 表现形式                 | 对应代码                                                     |
| ---------- | ------------------------ | ------------------------------------------------------------ |
| text       | 单行输入文本             | <input type=text" />                                         |
| password   | 密码输入框（不显示明文） | <input type="password"  />                                   |
| date       | 日期输入框               | <input type="date" />                                        |
| checkbox   | 复选框                   | <input type="checkbox" checked="checked" name='x' />         |
| radio      | 单选框                   | <input type="radio" name='x' />                              |
| submit     | 提交按钮                 | <input type="submit" value="提交" /> #发送浏览器上输入标签中的内容，配合form表单使用，页面会刷新 |
| reset      | 重置按钮                 | <input type="reset" value="重置"  />  #页面不会刷新，将所有输入的内容清空 |
| button     | 普通按钮                 | <input type="button" value="普通按钮"  />                    |
| hidden     | 隐藏输入框               | <input type="hidden"  />                                     |
| file       | 文本选择框               | <input type="file"  /> （等学了form表单之后再学这个）        |



总结

```
input文本输入框,input标签如果想将数据提交到后台,那么必须写name属性
input选择框,必须写name属性和value属性
input选择框,name值相同的算是一组选择框**
```

form表单标签

```
action属性:值是一个网址,将数据提交到哪个网址去
method属性:提交方法,默认是get,效果是在网址上可以看到提交的数据

```

注意,想通过form表单标签来提交用户输入的数据,必须在form表单里面写你的input标签,并且必须有个提交按钮,按钮有两种,1:  input标签type='submit', 2: botton按钮

select下拉框

```
<select name="" id="" multiple>
    <option value="1">沙河市</option>
    <option value="2">于辛庄市</option>
    <option value="3">回龙观市</option>

</select>

multiple属性,多选,name属性提交数据时的键,option标签中value属性的值需要写,是将来提交数据的真实数据
```

label标签 (标识一下标签的作用)

```
<label for="username">用户名:</label>
<!--用户名:-->
<input type="text" id="username">


<label>
    密码 <input type="password">
</label>

　　1.label 元素不会向用户呈现任何特殊效果。但是点击label标签里面的文本，那么和他关联的input标签就获得了光标，让你输入内容
　　2.<label> 标签的 for 属性值应当与相关元素的 id 属性值相同。
```

textarea标签,多行文本输入框

