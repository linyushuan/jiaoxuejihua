"""
设定一个理想数字比如：66，让用户输入数字，如果比66大，则显示猜测的结果大了；
如果比66小，则显示猜测的结果小了;只有等于66，显示猜测结果正确，然后退出循环。
"""
# age = 66
# input()
# if
# >
# print()
# <
# print()
# while

# age = 66
# while True:
#     num = int(input("请输入年龄:"))
#     if num > age:
#         print("大了")
#     elif num < age:
#         print("小了")
#     else:
#         print("对了")
#         break

"""
给用户三次猜测机会，如果三次之内猜测对了，
则显示猜测正确，退出循环，如果三次之内没有猜测正确，
则自动退出循环，并显示‘太笨了你....’。
"""
# count = 0
# age = 66
# while count < 3:
#     num = int(input("请输入年龄:"))
#     if num > age:
#         print("大了")
#     elif num < age:
#         print("小了")
#     else:
#         print("对了")
#         break
#     count += 1
# else:
#     print("太笨了你....")


"""
使用while循环输出 1 2 3 4 5 6 8 9 10
"""

# count = 1
# while count < 11:
#     if count == 7:
#         count += 1
#     print(count)
#     count += 1

"""
求1-100的所有数的和
"""
# num_sum = 0
# count = 1
# while count < 101:
#     num_sum = num_sum + count
#     count += 1
# print(num_sum)

# 输出 1-100 内的所有奇数
# count = 1
# while count < 101:
#     print(count)
#     count += 2

# count = 1
# while count < 101:
#     if count % 2 == 1:
#         print(count)
#     count += 1

"""
求+1-2+3-4+5 ... 99的所有数的和
"""
# count = 1
# j = 0
# o = 0
# while count < 100:
#     if count % 2 == 1:
#         j += count
#         # pass ...  # 占位
#     else:
#         o -= count
#     count += 1
# print(j+o)

# count = 1
# sum = 0
# while count < 100:
#     if count % 2 == 1:
#         sum += count
#         # pass ...  # 占位
#     else:
#         sum -= count
#     count += 1
# print(sum)

"""14.⽤户登陆（三次输错机会）且每次输错误时显示剩余错误次数（提示：使⽤字符串格式化）"""
# count = 3
# while count:
#     user = input("user")
#     pwd = input("pwd")
#     count -= 1
#     if user == "大黑哥" and pwd == "大黑哥dsb":
#         print("欢迎大黑哥回家!")
#         break
#     else:
#         print("账号或密码错误,剩余次数%s"%(count))


