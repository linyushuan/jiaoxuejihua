def func1():
    pass

class A:
    def func(self):
        pass

# 1. 通过打印函数名的方式区别什么是方法,什么是函数. (了解)
# print(func1)
# print(A.func)  # 通过类名调用的类中的实例方法叫做函数.
# obj = A()
# print(obj.func) # 通过对象调用的类中的实例方法叫方法.

# 2 可以借助模块判断是方法还是函数.

# from types import FunctionType
# from types import MethodType
#
# def func():
#     pass
#
#
# class A:
#     def func(self):
#         pass
#
# obj = A()


# print(isinstance(func,FunctionType))  # True
# print(isinstance(A.func,FunctionType))  # True
# print(isinstance(obj.func,FunctionType))  # False
# print(isinstance(obj.func,MethodType))  # True
# 总结:
# python 中一切皆对象, 类在某种意义上也是一个对象,python中自己定义的类,
# 以及大部分内置类,都是由type元类(构建类)实例化得来的.
# python 中一切皆对象, 函数在某种意义上也是一个对象,函数这个对象是从FunctionType这个类实例化出来的.
# python 中一切皆对象, 方法在某种意义上也是一个对象,方法这个对象是从MethodType这个类实例化出来的.


class A:
    @classmethod
    def func(cls,a):
        pass

    @staticmethod
    def func1():
        pass

# A.func(222)
# A.func()
# obj = A()
# obj.func()

# 总结: 如何判断类中的是方法还是函数.
# 函数都是显性传参,方法都是隐性传参.

