class A:
    pass
# obj = A()
# print(type('abc'))
# print(type([1,2,3]))
# print(type((22,33)))
#
# # type 获取对象从属于的类
# print(type(A))
# print(type(str))
# print(type(dict))

# python 中一切皆对象, 类在某种意义上也是一个对象,python中自己定义的类,
# 以及大部分内置类,都是由type元类(构建类)实例化得来的.

# type 与 object 的关系.
# print(type(object)) object类是type类的一个实例.
# object类是type类的父类.
# print(issubclass(type,object))