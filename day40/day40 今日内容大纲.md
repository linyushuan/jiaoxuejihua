今日内容

DBMS  数据库管理系统

mysql  RDBMS  关系型数据库管理系统

sql语句  结构化查询语句

永久修改字符集编码的方法

在mysql安装目录下创建一个my.ini(my.cnf)文件,写入下面的配置,然后重启服务端

```mysql
[client]
#设置mysql客户端默认字符集
default-character-set=utf8 

[mysql]
# 设置mysql客户端默认字符集
default-character-set=utf8 
user = 'root'
password = '123'

[mysqld]
#设置3306端口
port = 3306 
# 设置mysql的安装目录
basedir=E:\mysql-5.6.45-winx64 
# 设置mysql数据库的数据的存放目录
datadir=E:\mysql-5.6.45-winx64\data 
# 允许最大连接数
max_connections=200
# 服务端使用的字符集默认为8比特编码的latin1字符集
character-set-server=utf8
# 创建新表时将使用的默认存储引擎
default-storage-engine=INNODB
```

查看字符集编码的指令:

```
show variables like "%char%";
```

MySQL客户端连接服务端时的完整指令

```
mysql -h 127.0.0.1 -P 3306 -u root -p
```

如果密码忘了怎么办?

```
1 停掉MySQL服务端(net stop mysql)
2 切换到MySQL安装目录下的bin目录下,然后手动指定启动程序来启动mysql服务端,指令: mysqld.exe --skip-grant-tables
3 重新启动一个窗口,连接mysql服务端,
4 修改mysql库里面的user表里面的root用户记录的密码:
	update user set password = password('666') where user='root';
5 关掉mysqld服务端,指令:
	tasklist|findstr mysqld
	taskkill /F /PID 进程号
	
6 正常启动服务端(net start mysql)
```

修改密码的三种方式

```

方法1： 用SET PASSWORD命令 
　　　　首先登录MySQL，使用mysql自带的那个客户端连接上mysql。 
　　　　格式：mysql> set password for 用户名@localhost = password('新密码'); 
　　　　例子：mysql> set password for root@localhost = password('123'); 
　　　　
方法2：用mysqladmin  （因为我们将bin已经添加到环境变量了，这个mysqladmin也在bin目录下，所以可以直接使用这个mysqladmin功能，使用它来修改密码）

　　　　关于mysqladmin的介绍：是一个执行管理操作的客户端程序。它可以用来检查服务器的配置和当前状态、创建和删除数据库、修改用户密码等等的功能，虽然mysqladmin的很多功能通过使用MySQL自带的mysql客户端可以搞定，但是有时候使用mysqladmin操作会比较简单。
　　　　格式：mysqladmin -u用户名 -p旧密码 password 新密码 
　　　　例子：mysqladmin -uroot -p123456 password 123 　
　　　　
方法3：用UPDATE直接编辑那个自动的mysql库中的user表 
　　　　首先登录MySQL，连接上mysql服务端。 
　　　　mysql> use mysql;   use mysql的意思是切换到mysql这个库，这个库是所有的用户表和权限相关的表都在这个库里面，我们进入到这个库才能修改这个库里面的表。
　　　　mysql> update user set password=password('123') where user='root' and host='localhost';   其中password=password('123') 前面的password是变量，后面的password是mysql提供的给密码加密用的，我们最好不要明文的存密码，对吧，其中user是一个表，存着所有的mysql用户的信息。

　　　　mysql> flush privileges;  刷新权限，让其生效，否则不生效，修改不成功。

```

存储引擎

```
默认存储引擎 Innodb
查看存储引擎
show engines；
```



























