

# 昨日内容回顾

## ajax

```
   特点:异步请求和局部刷新
   
   $('#sub').click(function () {

        var uname = $('#username').val();
        var pwd = $('#password').val();
        // $.ajax({
        	  url:'{% url "login" %}',#}
        //    type:'get',
        //    success:function (res) {
        //        console.log(res);
        //    }
        //})

        $.ajax({
            url:'{% url "login" %}',
            type:'post',
            // data:{username:uname,password:pwd,csrfmiddlewaretoken:csrf},
            data:{username:uname,password:pwd},
            headers:{
                "X-CSRFToken":$.cookie('csrftoken'),
            },
            success:function (res) {
                console.log(res);
                if (res === '1'){
                    // $('.error').text('登录成功');
                    location.href = '/home/'; // http://127.0.0.1:8000/home/

                }else{
                    $('.error').text('用户名密码错误!');
                    
                }

            }

        })

    })



```



csrftoken

详述CSRF（Cross-site request forgery），中文名称：跨站请求伪造，也被称为：one click attack/session riding，缩写为：CSRF/XSRF。攻击者通过HTTP请求江数据传送到服务器，从而盗取回话的cookie。盗取回话cookie之后，攻击者不仅可以获取用户的信息，还可以修改该cookie关联的账户信息。

　　![img](https://img2018.cnblogs.com/blog/988061/201908/988061-20190804210821110-1699988322.png)

 

　　所以解决csrf攻击的最直接的办法就是生成一个随机的csrftoken值，保存在用户的页面上，每次请求都带着这个值过来完成校验。



### form表单

```
<form action="" method="post">
    {% csrf_token %}  // form表单里面加上这个标签,模板渲染之后就是一个input标签,type=hidden  name=csrfmiddlewaretoken  value='asdfasdfasdf'
    用户名: <input type="text" name="username">
    密码: <input type="password" name="password">
    <input type="submit">

</form>

```

### ajax过csrf认证

```
方式1
    $.ajax({
      url: "/cookie_ajax/",
      type: "POST",
      data: {
        "username": "chao",
        "password": 123456,
        "csrfmiddlewaretoken": $("[name = 'csrfmiddlewaretoken']").val()  // 使用jQuery取出csrfmiddlewaretoken的值，拼接到data中
      },
      success: function (data) {
        console.log(data);
      }
    })

方式2
	$.ajax({
        data: {csrfmiddlewaretoken: '{{ csrf_token }}' },
    });
方式3
	$.ajax({
 
		headers:{"X-CSRFToken":$.cookie('csrftoken')}, #其实在ajax里面还有一个参数是headers，自定制请求头，可以将csrf_token加在这里，我们发contenttype类型数据的时候，csrf_token就可以这样加
 
})

jquery操作cookie: https://www.cnblogs.com/clschao/articles/10480029.html

```



### form表单上传文件

```
<form action="" method="post" enctype="multipart/form-data">  别忘了enctype
    {% csrf_token %}
    用户名: <input type="text" name="username">
    密码: <input type="password" name="password">
    头像: <input type="file" name="file"> 

    <input type="submit">

</form>


views.py
def upload(request):

    if request.method == 'GET':
        print(settings.BASE_DIR) #/static/

        return render(request,'upload.html')

    else:
        print(request.POST)
        print(request.FILES)
        uname = request.POST.get('username')
        pwd = request.POST.get('password')

        file_obj = request.FILES.get('file')  #文件对象

        print(file_obj.name) #开班典礼.pptx,文件名称
		
        with open(file_obj.name,'wb') as f:
            # for i in file_obj:
            #     f.write(i)
            for chunk in file_obj.chunks():
                f.write(chunk)
        return HttpResponse('ok')


```



### ajax上传文件

```
  $('#sub').click(function () {
  
        var formdata = new FormData();
	    
        var uname = $('#username').val();
        var pwd = $('#password').val();

        var file_obj = $('[type=file]')[0].files[0]; // js获取文件对象

        formdata.append('username',uname);
        formdata.append('password',pwd);
        formdata.append('file',file_obj);

        $.ajax({
            url:'{% url "upload" %}',
            type:'post',
            // data:{username:uname,password:pwd,csrfmiddlewaretoken:csrf},
            //data:{username:uname,password:pwd},
            data:formdata,
            
            processData:false,  // 必须写
            contentType:false,  // 必须写

            headers:{
                "X-CSRFToken":$.cookie('csrftoken'),
            },
            success:function (res) {
                console.log(res);
                if (res === '1'){
                    // $('.error').text('登录成功');
                    location.href = '/home/'; // http://127.0.0.1:8000/home/

                }else{
                    $('.error').text('用户名密码错误!');
                }

            }

        })

    })
```



jsonresponse

```
from django.http import JsonResponse


        username = request.POST.get('username')
        pwd = request.POST.get('password')
        ret_data = {'status':None,'msg':None}
        print('>>>>>',request.POST)
        #<QueryDict: {'{"username":"123","password":"123"}': ['']}>
        if username == 'chao' and pwd == '123':
            ret_data['status'] = 1000  # 状态码
            ret_data['msg'] = '登录成功'


        else:
            ret_data['status'] = 1001  # 状态码
            ret_data['msg'] = '登录失败'

        # ret_data_json = json.dumps(ret_data,ensure_ascii=False)
        # return HttpResponse(ret_data_json,content_type='application/json')
        
        return JsonResponse(ret_data)
        
        ret_data = ['aa',22,'哈哈']

        # return HttpResponse(ret_data_json,content_type='application/json')
        return JsonResponse(ret_data,safe=False) #非字典类型数据需要给JsonResponse加上safe=False的参数

```

```
 $.ajax({
            url:'{% url "jsontest" %}',
            type:'post',
            // data:{username:uname,password:pwd,csrfmiddlewaretoken:csrf},
            //data:JSON.stringify({username:uname,password:pwd}),

            data:{username:uname,password:pwd},
            headers:{
                // contentType:'application/json',
                "X-CSRFToken":$.cookie('csrftoken'),
            },
            success:function (res) {
                {#console.log(res,typeof res); // statusmsg {"status": 1001, "msg": "登录失败"}#}
                {#var res = JSON.parse(res);  //-- json.loads()#}
                console.log(res,typeof res);  //直接就是反序列化之后的了
                //JSON.stringify()  -- json.dumps
                if (res.status === 1000){
                    // $('.error').text('登录成功');
                    location.href = '/home/'; // http://127.0.0.1:8000/home/

                }else{
                    $('.error').text(res.msg);
                }

            }

        })

```



# 今日内容

## 事务和锁

锁

```
mysql:  select * from book where id=1 for update;

begin;  start transaction;
	select * from t1 where id=1 for update;
commit

rollback;


django orm
	models.Book.objects.select_for_update().filter(id=1)

```



事务

```
from django.db import transaction

@transaction.atomic
def index(request):
	
	...


def index(request):
	...
	with transaction.atomic():
		xxxx
	...

```



### 中间件

```
process_request(self,request)
process_view(self, request, view_func, view_args, view_kwargs)
process_template_response(self,request,response)
process_exception(self, request, exception)
process_response(self, request, response)

```



请求生命周期

![img](https://img2018.cnblogs.com/blog/988061/201903/988061-20190307152249812-1922952163.png)

















































