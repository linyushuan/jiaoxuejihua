# 队列: 把队列理解成一个容器,这个容器可以承载一些数据,
# 队列的特性: 先进先出永远保持这个数据. FIFO 羽毛球筒.
# from multiprocessing import Queue
# q = Queue()
# def func():
#     print('in func')
# q.put(1)
# q.put('alex')
# q.put([1,2,3])
# q.put(func)
#
#
# print(q.get())
# print(q.get())
# print(q.get())
# f = q.get()
# f()


# from multiprocessing import Queue
# q = Queue(3)
#
# q.put(1)
# q.put('alex')
# q.put([1,2,3])
# # q.put(5555)  # 当队列满了时,在进程put数据就会阻塞.
# # q.get()
#
# print(q.get())
# print(q.get())
# print(q.get())
# print(q.get())  # 当数据取完时,在进程get数据也会出现阻塞,直到某一个进程put数据.


from multiprocessing import Queue
q = Queue(3)  # maxsize

q.put(1)
q.put('alex')
q.put([1,2,3])
# q.put(5555,block=False)
#
print(q.get())
print(q.get())
print(q.get())
print(q.get(timeout=3))  # 阻塞3秒,3秒之后还阻塞直接报错.
# print(q.get(block=False))

# block=False 只要遇到阻塞就会报错.


# 第一个作业: 将抢票系统变成队列方式完成.

