# 算数运算符
# +
# -
# *
# / python2获取的值是整数，python3获取的是浮点数（小数2.5）
# print(5/2)
# //(整除-- 地板除)
# print(5 // 2)
# ** 幂（次方）
# print(3**2)
# % 模 (取余)
# print(5 % 2)


# 比较运算符
# >
# <
# == (等于)
# != (不等于)
# >=
# <=


# 赋值运算符
# = 赋值
# += 自加
# a = 10
# a += 1 # a = a + 1
# print(a)
# -= 自减
# *= 自乘
# a = 10
# a *= 2  # a = a * 2
# print(a)
# /=
# //=
# **=
# %=

# 逻辑运算符
# and (与/和)
# or （或）
# not （非）

# print(3 and 4)
# print(0 and 4)
# print(0 and False)

# and 都为真的时候取and后边的值
# and 都为假的时候取and前面的值
# and 一真一假取假的

# print(3 and 5 and 9 and 0 and False)
# print(5 and False and 9 and 0)
# print(1 and 2 and 5 and 9 and 6)

# print(1 or 0)
# print(1 or 2)
# print(0 or False)

# or 都为真的时候取or前边的值
# or 都为假的时候取or后面的值
# or 一真一假取真的

# print(1 or 9 or 4 or 0 or 9)
# print(not False)

# () > not > and > or
# 从左向右执行
# print(9 and 1 or not False and 8 or 0 and 7 and False)


# 成员运算符
# in  存在
# not in 不存在

# s = "alexdsb"
# if "sb" not in s:
#     print(True)
# else:
#     print(False)

