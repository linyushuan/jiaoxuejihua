# 什么是基础类型?

# 基础类型是干嘛用的?

# 基础类型在那用?

# 56 整型 用于计算和比较的
# 56 + 3
# 56 - 2
# 56 * 2
# 56 / 6

# "你好" 字符串
# a = "黑哥"
# b = "b哥"
# c = "春哥"
# print(a + c + b)   # 字符串拼接
# d = "水银"
# print(d * 8)       # 字符串乘法运算

a = """
你
好
黑
哥
今
天
比
教
。。。"""
# b = '我好'
# c = "大家好"
print(a)
# print(b)
# print(c)
# '''my name's "meet"'''

# 布尔值
# True  真
# False 假
# print(3<2)
