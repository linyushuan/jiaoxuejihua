if - 如果 在python是关键字

if True 如果是真的，告诉大家这是真的

单if
关键字 空格 条件 冒号
缩进 结果

print(1)
if 3>2:
    print(3)
print(2)

if else 二选一
如果 空格 条件 冒号
缩进 结果
否则 冒号
缩进 结果

n = input("请输入数字:")
if 3<int(n):
    print(1)
else:
    print(2)
print(6)

if elif elif 多选一或零

print(110)
if 3<2: # 如果
    print("A")
elif 3>8: # 在如果
    print("B")
elif 5<0:
    print("C")
print(112)

if elif else 多选一

if 3 == 2:
    print(1)
elif 3<2:
    print(3)
elif 3>10:
    print(4)
else:
    print(9)

if if if 多选

if 3>2:
    print("A")
print(456)
if 3<6:
    print("B")

if嵌套
sex = "女"
age = 35
if sex == "女":
    if age == 35:
        print("进来坐一坐")
    else:
        print("你去隔壁找春生")
else:
    print("你去对门找alex")


1.用户输入账号
2.用户输入密码
3.判断用户的账号是不是alex
4.如果账号是alex在继续判断密码是不是alexdsb
5.账号和密码都正确提示用户alex就是一个dsb
6.如果账号正确密码错误提示密码错误
7.如果账号错误提示账号错误


user = input("请输入账号：")
pwd = input("请输入密码：")
if user == "alex":
    if pwd == "alexdsb":
        print("alex就是一个dsb")
    else:
        print("密码错误！")
else:
    print("账号错误！")

user = input("请输入账号：")
pwd = input("请输入密码：")

# and # 和 and前边的内容为真并且and 后边的内容为真才是真

if pwd == "alexdsb" and user == "alex":
    print("alex就是一个dsb")
else:
    print("密码或账户错误！")
