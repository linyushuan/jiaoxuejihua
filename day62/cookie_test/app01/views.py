from django.shortcuts import render,HttpResponse

# Create your views here.

def index(request):
    data = HttpResponse('相应')

    data.set_cookie('a1','1',path='/')
    data.set_cookie('a2','2',path='')
    data.set_cookie('a3','3',path='/test/')

    return data


def home(request):
    return HttpResponse('HOME')

def test(request):
    return HttpResponse('TEST')

def test_api(request):
    return HttpResponse('TEST API')