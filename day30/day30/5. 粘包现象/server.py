# 1. 粘包第一种： send的数据过大，大于对方recv的上限时，对方第二次recv时，会接收上一次没有recv完的剩余的数据。
# import socket
# import subprocess
# phone = socket.socket()
#
# phone.bind(('127.0.0.1',8848))
#
# phone.listen(2)
# # listen: 2 允许有两个客户端加到半链接池，超过两个则会报错
#
# while 1:
#     conn,addr = phone.accept()  # 等待客户端链接我,阻塞状态中
#     # print(f'链接来了: {conn,addr}')
#
#     while 1:
#         try:
#
#             from_client_data = conn.recv(1024)  # 最多接受1024字节
#
#
#             if from_client_data.upper() == b'Q':
#                 print('客户端正常退出聊天了')
#                 break
#
#             obj = subprocess.Popen(from_client_data.decode('utf-8'),
#                                    shell=True,
#                                    stdout=subprocess.PIPE,
#                                    stderr=subprocess.PIPE,
#
#                                    )
#             result = obj.stdout.read() + obj.stderr.read()
#             print(f'总字节数：{len(result)}')
#             conn.send(result)
#         except ConnectionResetError:
#             print('客户端链接中断了')
#             break
#     conn.close()
# phone.close()



# s1 = '太白jx'
# # print(len(s1))
# b1 = s1.encode('utf-8')
# # print(b1)
# print(len(b1))


'''
         客户端                          服务端

第一次：  ipconfig                       317字节
         300个字节                       17个字节
         

         客户端                          服务端

第二次：   dir                           376字节
          17字节                        376字节
         

'''


# 2. 连续短暂的send多次(数据量很小),你的数据会统一发送出去.

# import socket
#
# phone = socket.socket()
#
# phone.bind(('127.0.0.1',8848))
#
# phone.listen(5)
#
#
# conn,addr = phone.accept()  # 等待客户端链接我,阻塞状态中
#
# from_client_data = conn.recv(1024)  # 最多接受1024字节
# print(f'来自客户端{addr}消息:{from_client_data.decode("utf-8")}')
# conn.close()
# phone.close()


# 展示一些收发的问题。








