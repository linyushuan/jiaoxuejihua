from django.shortcuts import render,HttpResponse,redirect

# Create your views here.

# 装饰器
def wrapper(func):
    def inner(*args, **kwargs):
        print(11111)
        ret = func(*args, **kwargs)
        print(22222)

        return ret

    return inner


@wrapper
def index(request):
    print('xxxxx')
    return HttpResponse('indexxxxxxxxx')
    # return render(request,'index.html')


def home(request):
    # print(request)  #<WSGIRequest: GET '/home/'>
    # # print(dir(request))
    #
    # print(request.path) #/home/  纯路径
    # print(request.path_info) #/home/  纯路径
    # print(request.get_full_path()) #/home/?a=1&b=2  全路径(不包含ip地址和端口)

    # print(request.META)  #请求头相关数据,是一个字典

    # print(request.method)  #GET

    # print(request.GET)
    # print(request.POST)

    print(request.body)  #GET请求body里面没有数据  post请求原始数据b'username=xx&password=22'
    # return HttpResponse('xxxxx')  #回复字符串用的额
    # return render(request,'home.html') #回复html页面 用的
    # return redirect('/index/')  #重定向  redirect(路径)


    # name = 'chao'



    # t = Template()
    # t.render({'xx':[11,22,33]})
    # return render(request, 'home.html',{'namxx':name,})  #模板渲染,这是在回复给浏览器之前做的事情










#CBV
from django.views import View
from django.utils.decorators import method_decorator


@method_decorator(wrapper,name='get')  # 方式3
# @method_decorator(wrapper,name='post')
class LoginView(View):
    #
    # @method_decorator(wrapper) #方式2
    # def dispatch(self, request, *args, **kwargs):
    #     print('xx请求来啦!!!!')
    #
    #     ret = super().dispatch(request, *args, **kwargs)
    #
    #     print('请求处理的逻辑已经结束啦!!!')
    #     return ret

    # 处理get请求直接定义get方法,不需要自己判断请求方法了,源码中用dispatch方法中使用了反射来处理的
    # @method_decorator(wrapper)  #方式1
    def get(self,request):
        print('小小小小')
        return render(request,'login.html')

    def post(self,request):
        print(request.POST)

        return HttpResponse('登录成功')
























