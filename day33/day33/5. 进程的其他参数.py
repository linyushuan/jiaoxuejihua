# from multiprocessing import Process
# import time
#
# def task(name):
#     print(f'{name} is running')
#     time.sleep(2)
#     print(f'{name} is gone')
#
#
#
# if __name__ == '__main__':
#     # 在windows环境下, 开启进程必须在 __name__ == '__main__' 下面
#     # p = Process(target=task,args=('常鑫',))  # 创建一个进程对象
#     p = Process(target=task,args=('常鑫',),name='alex')  # 创建一个进程对象
#     p.start()
#     # time.sleep(1)
#     # p.terminate()  # 杀死子进程  ***
#     # p.join()  # ***
#     # time.sleep(0.5)
#     # print(p.is_alive())   # ***
#     # print(p.name)
#     p.name = 'sb'
#     print(p.name)
#     print('==主开始')

