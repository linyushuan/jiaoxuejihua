



# 昨日内容回顾

### JSON对象

```
var a = {'name':'太白','age':89};
序列化:var b = JSON.stringify(a);
反序列化:var c = JSON.parse(b);
```



### RegExp对象

```

var reg1 = new RegExp("^[a-zA-Z][a-zA-Z0-9_]{5,11}$"); 
// 简写方式
var reg2 = /^[a-zA-Z][a-zA-Z0-9_]{5,11}$/; 

坑:
	reg2.test(); 什么也不填写,会默认成reg2.test('undefined');
	如果'undefined'满足你的正则要求,就返回true


字符串使用正则是的一些方法
var s2 = "hello world";
s2.match(/o/);  匹配元素
s2.match(/o/g);  加上g是全局匹配
s2.search(/o/);  找符合正则规则的字符串的索引位置
s2.split(/o/);  用符合正则的字符串进行分割

var s3 = 'Alex is A xiaosb';
s3.replace(/a/gi,'DSB');  替换,g全局替换,i不区分大小写


正则加g之后,进行test测试需要注意的问题

var reg3 = /a/g;
var s1 = 'alex is a xiaosb';
reg3.lastIndex -- 0
reg3.test(s1); -- true
reg3.lastIndex -- 1
reg3.test(s1); -- true
reg3.lastIndex -- 9
reg3.test(s1); -- true
reg3.lastIndex -- 13

reg3.test(s1); -- false

置零:
	reg3.lastIndex = 0
```



### Math对象

```

Math.abs(x)      返回数的绝对值。
exp(x)      返回 e 的指数。
floor(x)    小数部分进行直接舍去。
log(x)      返回数的自然对数（底为e）。
max(x,y)    返回 x 和 y 中的最高值。
min(x,y)    返回 x 和 y 中的最低值。
pow(x,y)    返回 x 的 y 次幂。
random()    返回 0 ~ 1 之间的随机数。
round(x)    把数四舍五入为最接近的整数。
sin(x)      返回数的正弦。
sqrt(x)     返回数的平方根。
tan(x)      返回角的正切。
```



# BOM对象

```
location对象
    location.href  获取URL
    location.href="URL" // 跳转到指定页面
    location.reload() 重新加载页面,就是刷新一下页面


定时器
    1. setTimeOut()  一段时间之后执行某个内容,执行一次
        示例 
            var a = setTimeout(function f1(){confirm("are you ok?");},3000);
            var a = setTimeout("confirm('xxxx')",3000);  单位毫秒
        清除计时器
            clearTimeout(a);  
    2.setInterval()  每隔一段时间执行一次,重复执行 
        var b = setInterval('confirm("xxxx")',3000);单位毫秒
        清除计时器
            clearInterval(b);

```



# DOM对象

## 查找标签

### 直接查找

```
document.getElementById           根据ID获取一个标签
document.getElementsByClassName   根据class属性获取（可以获取多个元素，所以返回的是一个数组）
document.getElementsByTagName     根据标签名获取标签合集

示例:
	<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Title</title>
    </head>
    <body>

        <div class="c1" id="d1">
            are you ok?

        </div>

        <div class="c1 c2">
            div2
        </div>
    </body>
    </html>

操作:
	var divEle = document.getElementById('d1');
	var divEle = document.getElementsByClassName('c1');
	var divEle = document.getElementsByTagName('div');
```



### 间接查找

```
parentElement            父节点标签元素
children                 所有子标签
firstElementChild        第一个子标签元素
lastElementChild         最后一个子标签元素
nextElementSibling       下一个兄弟标签元素
previousElementSibling   上一个兄弟标签元素
示例:
	<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Title</title>
    </head>
    <body>

        <div class="c1" id="d1">
            are you ok?

            <span id="s1">span1</span>
            <span id="s2">span2</span>
            <span id="s3">span3</span>
        </div>

        <div class="c1 c2">
            div2
        </div>
    </body>

操作:
	var divEle = document.getElementById('d1');
	找父级:divEle.parentElement;
	找儿子们:divEle.children;
	找第一个儿子:divEle.firstElementChild;
	找最后一个儿子:divEle.lastElementChild;
	找下一个兄弟:divEle.nextElementSibling;
```



### 标签操作

```
创建标签:重点
	var aEle = document.createElement('a');

添加标签
    追加一个子节点（作为最后的子节点）
    somenode.appendChild(newnode)；
	示例:
		var divEle = document.getElementById('d1')
		divEle.appendChild(aEle)
	
	
    把增加的节点放到某个节点的前边。
    somenode.insertBefore(newnode,某个节点);
	示例:
		var divEle = document.getElementById('d1'); 找到父级标签div
		var a = document.createElement('a');  创建a标签
		a.innerText = 'baidu';  添加文本内容
		var span2 = document.getElementById('s2'); 找到div的子标签span2
		divEle.insertBefore(a,span2); 将a添加到span2的前面
		
html文件代码:
	<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Title</title>
    </head>
    <body>

    <div class="c1" id="d1">
        are you ok?

        <span id="s1">span1</span>
        <span id="s2">span2</span>
        <span id="s3">span3</span>
    </div>

    <div class="c1 c2">
        div2
    </div>

    </body>
    </html>
		
```



删除节点

```
获得要删除的元素，通过父元素调用该方法删除。
somenode.removeChild(要删除的节点)
示例: 删除span2标签
    var divEle = document.getElementById('d1');
    var span2 = document.getElementById('s2');
    divEle.removeChild(span2);
```

替换节点：

```

somenode.replaceChild(newnode, 某个节点);
somenode是父级标签，然后找到这个父标签里面的要被替换的子标签，然后用新的标签将该子标签替换掉
```



### 文本节点操作

```
var divEle = document.getElementById("d1")
divEle.innerText  #输入这个指令，一执行就能获取该标签和内部所有标签的文本内容
divEle.innerHTML  #获取的是该标签内的所有内容，包括文本和标签
取值示例:
	div2.innerText;  不识别标签
    	"are you ok? span1 span2 span3"
    div2.innerHTML;  识别标签
        "
            are you ok?

            <span id="s1">span1</span>
            <span id="s2">span2</span>
            <span id="s3">span3</span>
        "
设置值:
	var div1 = document.getElementById('d1');
    div1.innerText = 'xxx';
    div1.innerText = '<a href="">百度</a>';
	div1.innerHTML = '<a href="">百度</a>';
```



### 属性操作

```
var divEle = document.getElementById("d1");
divEle.setAttribute("age","18")  #比较规范的写法
divEle.getAttribute("age")
divEle.removeAttribute("age")

// 自带的属性还可以直接.属性名来获取和设置，如果是你自定义的属性，是不能通过.来获取属性值的
imgEle.src
imgEle.src="..."

示例:
	<a href="http://www.baidu.com">百度</a>
	操作
		var a = document.getElementsByTagName('a');
		a[0].href;  获取值
		a[0].href = 'xxx'; 设置值
```



### 获取值操作

```
输入框 input
	获取值
		var inpEle = document.getElementById('username');
		inpEle.value;  
	设置值
		inpEle.value = 'alexDsb';

select选择框
	获取值
		var selEle = document.getElementById('select1');
		selEle.value;
     设置值
     	selEle.value = '1';
     	

```



### 类操作

```
className  获取所有样式类名(字符串)

首先获取标签对象
标签对象.classList; 标签对象所有的class类值

标签对象.classList.remove(cls)  删除指定类
classList.add(cls)  添加类
classList.contains(cls)  存在返回true，否则返回false
classList.toggle(cls)  存在就删除，否则添加，toggle的意思是切换，有了就给你删除，如果没有就给你加一个


示例:
	var divEle = document.getElementById('d1');
	divEle.classList.toggle('cc2');  
	var a = setInterval("divEle.classList.toggle('cc2');",30);

	判断有没有这个类值的方法
		var divEle = document.getElementById('d1');	
		divEle.classList.contains('cc1');
```



### css设置

```
1.对于没有中横线的CSS属性一般直接使用style.属性名即可。
2.对含有中横线的CSS属性，将中横线后面的第一个字母换成大写即可

设置值:
	divEle.style.backgroundColor = 'yellow';
获取值
	divEle.style.backgroundColor;
```



## 事件

```
简单示例:
	<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Title</title>

        <style>
            .cc1 {
                width: 100px;
                height: 100px;
                background-color: red;
            }
            .cc2{
                background-color: green;
            }

        </style>
    </head>
    <body>

    <div class="cc1 xx xx2" id="d1">

    </div>


    <script>
        var divEle = document.getElementById('d1');
        divEle.onclick = function () {
            divEle.style.backgroundColor = 'purple';
        }


    </script>
    </body>
    </html>
```



### 绑定事件的方式

```
方式1:
    <script>
        var divEle = document.getElementById('d1');  1.找到标签
        divEle.onclick = function () {       2.给标签绑定事件
            divEle.style.backgroundColor = 'purple';
        }
    </script>
    
    	下面的this表示当前点击的标签
        var divEle = document.getElementById('d1');
        divEle.onclick = function () {
            this.style.backgroundColor = 'purple';
        }
    
方式2
	标签属性写事件名称=某个函数();
	<div class="cc1 xx xx2" id="d1" onclick="f1();"></div>
	
    <script>
    	js里面定义这个函数
        function f1() {
            var divEle = document.getElementById('d1');
            divEle.style.backgroundColor = 'purple';
        }
    </script>
    
    
    获取当前操作标签示例,this标签当前点击的标签
    	<div class="cc1 xx xx2" id="d1" onclick="f1(this);"></div>
        function f1(ths) {
            ths.style.backgroundColor = 'purple';
        }
    
```



# 今日内容

### jquery引入

```
下载链接：jQuery官网  https://jquery.com/
中文文档：jQuery AP中文文档


<script src="jquery.js"></script>

<script>
</script>

第二种方式,网络地址引入
<!--<script src="https://cdn.bootcss.com/jquery/3.4.1/jquery.js"></script>-->

```

### 下载

![1568768586542](C:\Users\Administrator\Desktop\assets\1568768586542.png)

![1568768603513](C:\Users\Administrator\Desktop\assets\1568768603513.png)



```
jquey方法找到的标签对象称为jquery对象
原生js找到的标签对象称之为DOM对象
dom对象只能调用dom对象的方法,jquery对象只能用jquery方法,不能互通
```



jquery对象和dom对象互相转换

```
/jQuery对象转成DOM对象，通过一个jQuery对象+[0]索引零，就变成了DOM对象，就可以使用JS的代码方法了，DOM对象转换成jQuery对象：$(DOM对象)，通过$符号包裹一下就可以了
jquery对象 转换成 dom对象 :  $('#d1')[0] -- dom对象
dom对象转换为jquery对象 :  $(dom对象) -- jquery对象

```



## 标签查找

### 基础选择器

```
　　　　基本选择器（同css）
　　　　　　id选择器：

$("#id")  #不管找什么标签，用什么选择器，都必须要写$("")，引号里面再写选择器，通过jQuery找到的标签对象就是一个jQuery对象，用原生JS找到的标签对象叫做DOM对象，看我们上面的jQuery对象部分的内容
　　　　　　标签选择器：$("tagName")
　　　　　　class选择器：$(".className")
　　　　　　配合使用：$("div.c1")  // 找到有c1 class类的div标签
　　　　　　所有元素选择器：$("*")
　　　　　　组合选择器$("#id, .className, tagName")
　　　　

```

### 层级选择器：（同css）

```
x和y可以为任意选择器

$("x y");// x的所有后代y（子子孙孙）
$("x > y");// x的所有儿子y（儿子）
$("x + y")// 找到所有紧挨在x后面的y
$("x ~ y")// x之后所有的兄弟y
```

### 基本筛选器

```
:first // 第一个
:last // 最后一个
:eq(index)// 索引等于index的那个元素
:even // 匹配所有索引值为偶数的元素，从 0 开始计数
:odd // 匹配所有索引值为奇数的元素，从 0 开始计数
:gt(index)// 匹配所有大于给定索引值的元素
:lt(index)// 匹配所有小于给定索引值的元素
:not(元素选择器)// 移除所有满足not条件的标签
:has(元素选择器)// 选取所有包含一个或多个标签在其内的标签(指的是从后代元素找)
示例写法:
	$('li:has(span)');
	
$("div:has(h1)")// 找到所有后代中有h1标签的div标签，意思是首先找到所有div标签，把这些div标签的后代中有h1的div标签筛选出来
$("div:has(.c1)")// 找到所有后代中有c1样式类（类属性class='c1'）的div标签
$("li:not(.c1)")// 找到所有不包含c1样式类的li标签
$("li:not(:has(a))")// 找到所有后代中不含a标签的li标签
```



简单绑定事件的示例

```
// 绑定事件的方法
$('#btn').click(function () {
    $('.mode')[0].classList.remove('hide');
    $('.shadow')[0].classList.remove('hide');
	jquery写法:
	$('.mode,.shadow').removeClass('hide');
})
```

### 属性选择器

```
[attribute]
[attribute=value]// 属性等于
[attribute!=value]// 属性不等于
示例:
	$('[title]')

// 示例,多用于input标签
<input type="text">
<input type="password">
<input type="checkbox">
$("input[type='checkbox']");// 取到checkbox类型的input标签
$("input[type!='text']");// 取到类型不是text的input标签
```



### 表单筛选器

```
:text
:password
:file
:radio
:checkbox

:submit
:reset
:button
简单示例:
	
    <div>
        用户名: <input type="text">
    </div>

    <div>
        密码: <input type="password">
    </div>

    <div>
        sex:
        <input type="radio" name="sex">男
        <input type="radio" name="sex">女
        <input type="radio" name="sex">不详
    </div>
    
    找到type为text的input标签:$(':text')
    
```



### 表单对象属性

```
:enabled
:disabled
:checked
:selected

示例:
	<div>
        用户名: <input type="text">
    </div>

    <div>
        密码: <input type="password" disabled>
    </div>
	
    <div>
        sex:
        <input type="radio" name="sex">男
        <input type="radio" name="sex">女
        <input type="radio" name="sex">不详
    </div>
    <select name="" id="">

        <option value="1">怼姐</option>
        <option value="2">珊姐</option>
        <option value="3">华丽</option>
        <option value="4">亚索</option>

    </select>
    操作:
    找到可以用的标签 -- $(':enabled')
    找select标签被选中的option标签  $(':selected')
```

### 链式表达式

```
<ul>
    <li>陈硕</li>
    <li>子文</li>
    <li class="c1">李业</li>
    <li>吕卫贺</li>
    <li>峡谷先锋珊姐</li>
    <li class="c2">怼姐</li>
    <li>骚强</li>

</ul>

操作		$('li:first').next().css('color','green').next().css('color','red');
```



筛选器方法

```
下一个
    $("#id").next()
    $("#id").nextAll()
    $("#id").nextUntil("#i2") #直到找到id为i2的标签就结束查找，不包含它
上一个
	$("#id").prev()
    $("#id").prevAll()
    $("#id").prevUntil("#i2")

<ul>
    <li>陈硕</li>
    <li>子文</li>
    <li class="c1">李业</li>
    <li>吕卫贺</li>
    <li>峡谷先锋珊姐</li>
    <li class="c2">怼姐</li>
    <li>骚强</li>

</ul>

示例:
	$('li:first').next()  找到第一个标签的下一个标签
	$('.c2').prevUntil('.c1');

```

```
父元素
	$("#id").parent()
    $("#id").parents()  // 查找当前元素的所有的父辈元素（爷爷辈、祖先辈都找到）
    $("#id").parentsUntil('body') // 查找当前元素的所有的父辈元素，直到遇到匹配的那个元素为止，这里直到body标签，不包含body标签，基本选择器都可以放到这里面使用。
    
儿子和兄弟元素：
    $("#id").children();// 儿子们
    $("#id").siblings();// 兄弟们，不包含自己，.siblings('#id')，可以在添加选择器进行进一步筛选
```

```
查找find,找的是后代
	$("div").find("p")  -- 等价于$("div p")
筛选filter
	$("div").filter(".c1") -- 等价于 $("div.c1")  // 从结果集中过滤出有c1样式类的，从所有的div标签中过滤出有class='c1'属性的div，和find不同，find是找div标签的子子孙孙中找到一个符合条件的标签
```

```
.first() // 获取匹配的第一个元素
.last() // 获取匹配的最后一个元素
.not() // 从匹配元素的集合中删除与指定表达式匹配的元素
.has() // 保留包含特定后代的元素，去掉那些不含有指定后代的元素。
.eq() // 索引值等于指定值的元素
```





## 操作标签

### 样式操作

```
样式类操作
	addClass();// 添加指定的CSS类名。
    removeClass();// 移除指定的CSS类名。
    hasClass();// 判断样式存不存在
    toggleClass();// 切换CSS类名，如果有就移除，如果没有就添加。
```

### css操作

```
单个方式:$('div').css('background-color','green');
多个方式:$('div').css({'background-color':'yellow','width':'400px'});
```

### 位置操作

```
offset()// 获取匹配元素在当前窗口的相对偏移或设置元素位置
position()// 获取匹配元素相对父元素的偏移，不能设置位置

	.offset()方法允许我们检索一个元素相对于文档（document）的当前位置。和 .position()的差别在于： .position()获取相对于它最近的具有相对位置(position:relative或position:absolute)的父级元素的距离，如果找不到这样的元素，则返回相对于浏览器的距离

示例:
	$('.c2').offset(); 查看位置
	$('.c2').offset({top:100,left:200}); 设置位置
	
	
	代码:
	<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Title</title>

        <style>
            /*body{*/
            /*    margin: 0;*/
            /*}*/
            .c1{
                background-color: red;
                height: 100px;
                width: 100px;
                display: inline-block;

            }
            .c2{
                background-color: green;
                height: 100px;
                width: 100px;
                display: inline-block;
            }
        </style>
    </head>
    <body>
    <div class="cc">
        <div class="c1"></div><div class="c2"></div>
    </div>
    <script src="jquey.js"></script>
    </body>
    </html>
```

```
$(window).scrollTop()  //滚轮向下移动的距离
$(window).scrollLeft() //滚轮向左移动的距离
```



### 尺寸

```
height() //盒子模型content的大小，就是我们设置的标签的高度和宽度
width()
innerHeight() //内容content高度 + 两个padding的高度
innerWidth()
outerHeight() //内容高度 + 两个padding的高度 + 两个border的高度，不包括margin的高度，因为margin不是标签的，是标签和标签之间的距离
outerWidth()
示例:
	<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Title</title>

        <style>
            .c1{
                width: 100px;
                height: 100px;
                padding: 20px 30px;
                border: 2px solid red;

            }



        </style>

    </head>
    <body>

    <div class="c1"></div>


    </body>
    <script src="jquery.js"></script>
    </html>
    
    操作:
    	$('.c1').height();
        $('.c1').width();
        $('.c1').innerWidth();
        $('.c1').outerWidth();
```

### 文本操作

```
html()// 取得第一个匹配元素的html内容，包含标签内容
html(val)// 设置所有匹配元素的html内容，识别标签，能够表现出标签的效果
text()// 取得所有匹配元素的内容，只有文本内容，没有标签
text(val)// 设置所有匹配元素的内容，不识别标签，将标签作为文本插入进去
示例:
	取值	
		$('.c1').html();
		$('.c1').text();
	设置值
        $('.c1').text('<a href="">百度</a>');
        $('.c1').html('<a href="">百度</a>');
```













































































































