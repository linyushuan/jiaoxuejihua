# 1.函数名的第一类对象及使用
# 1.1 函数名可以当作值赋值给变量
# 1.2 函数名可以当作容器中元素
# 1.3 函数名可以当作另一个函数的参数
# 1.4 函数名可以当作返回值

# 2. 可迭代对象:
    # 使用灵活,每个可迭代对象都有私有的方法
    # 可以直观的查看值

    # 迭代器:
    #     节省内存,惰性机制
    #     不能逆行
    #     一次性

# 迭代器:具有__iter__()和__next__()方法就是迭代器
# 可迭代对象: 具有__iter__()方法就是可迭代对象

# f - 格式化
# f,F
# f"{表达式}"
# f"{函数调用}"
# f"{列表取值}"
# f"{字典取值}"
# f"{数字计算}"
# f"{变量名}"
# f"{字符串}"

# 递归:
# 1.不断调用自己本身
# 2.有明确的结束条件

# 目前你拆开了来理解
# 一递 一归
