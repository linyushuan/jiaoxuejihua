# lst = []
# for i in range(20):
#     lst.append(i)
# print(lst)

# list推导式
# print([i for i in range(20)])

# 循环模式
# [变量 for i in range(20)]
# print([i+1 for i in range(10)])

# 筛选模式
# lst = []
# for i in range(20):
#     if i % 2 == 0:
#         lst.append(i)
# print(lst)

# print([i for i in range(20) if i % 2 == 0])
# [变量(加工后的变量) for循环 加工方式]

# print([i for i in range(50) if i % 2 == 1])
# print([i for i in range(1,50,2)])

# 生成器表达式:

# 循环模式:
# g = (i for i in range(20))
# print(next(g))
# print(next(g))
# print(next(g))

# print(list((i for i in range(20))))

# 筛选模式
# g = (i for i in range(50) if i % 2 == 1)
# for i in g:
#     print(i)

# 字典推导式:(了解)
# print({i:i+1 for i in range(10)})
# print({i:i+1 for i in range(10) if i % 2 == 0})
# {键:值 for循环 加工条件}

# 集合推导式:(了解)
# print({i for i in range(10)})
# print({i for i in range(10) if i % 2 == 0})

# list:
# [变量(加工后的变量) for循环]
# [变量(加工后的变量) for循环 加工条件]

# 生成器表达式:
# (变量(加工后的变量) for循环)
# (变量(加工后的变量) for循环 加工条件)

# 字典推导式:
# {键:值 for循环 加工条件}

# 集合推导式:
# {变量(加工后的变量) for循环 加工条件}

# 将10以内所有整数的平方写入列表
# 100以内所有的偶数写入列表.
# 从python1期到python24期写入列表lst
# print([f"pyhton{i}期" for i in range(1,25)])

# 将这个列表中大于3的元素留下来。
# print([i for i in [1,2,3,23,1234,4235,] if i > 3])