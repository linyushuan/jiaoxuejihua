# 1.内置函数二
# zip
# sorted()    高阶
# filter()    高阶
# reversed()
# map()       高阶
# abs()
# max()       高阶
# min()       高阶
# len()
# range()
# sum()
# print()
# list()
# dict(a=1,b=2)
# print(dict([(1,2),(2,3),(4,5)]))
# enumerate()
# reduce       高阶
# format() b d o x

# 2.匿名函数
# lambda x:x
# def   形参 返回值
# 返回值必须要写,返回值只能返回一个数据类型
# 形参可以不写
# 匿名函数的名字就是lambda
# lambda x,y:x+y

# 3.闭包
# def func():
#     a = 1
#     def foo():
#         print(a)
#     return foo
# func()()

# 1.在嵌套函数内使用非全局变量且不是本层变量

# 闭包的另一种形式
# def func(a):
#     def foo():
#         print(a)
#     return foo
# ret = func(2)
# print(ret.__closure__)


# 使用的全局变量,就不是闭包了
# a = 1
# def func():
#     def foo():
#         print(a)
#     return foo
# ret = func()
# print(ret.__closure__)